import json
import os
from flask import Flask, jsonify, current_app
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
from sqlalchemy import func
from sqlalchemy.sql.expression import cast
from datetime import date


class Sql():
            
    def __init__(self, app):
        #Find URI to postgres database. Refer to app.yaml for the URI
        self.app = app
        app.config["SQLALCHEMY_DATABASE_URI"] = os.environ["SQLALCHEMY_DATABASE_URI"]
        app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
            
        db = SQLAlchemy(app)
        self.db = db

        db.Model.metadata.reflect(db.engine)

        class Countries(db.Model):
            __table__ = db.Model.metadata.tables["countries"]
            def serialize(self):
                return {
                    "code": "{}".format(self.code),
                    "name": "{}".format(self.name),
                    "capital": "{}".format(self.capital),
                    "region": "{}".format(self.region),
                    "subregion": "{}".format(self.subregion),
                    "population": "{}".format(self.population),
                    "area": "{}".format(self.area),
                    "flag": "{}".format(self.flag),
                    "life expectancy": "{}".format(self.lifeexpectancy),
                    "mortality": "{}".format(self.mortality)
                }
        class Organizations(db.Model):
            __table__ = db.Model.metadata.tables["organizations"]
            def serialize(self):
                return {
                    "org_id": "{}".format(self.id),
                    "name": "{}".format(self.name),
                    "mission": "{}".format(self.mission),
                    "address": "{}".format(self.addressline),
                    "citystatezipcountry": "{}".format(self.citystatezipcountry),
                    "contact name": "{}".format(self.contactname),
                    "contact email": "{}".format(self.contactemail),
                    "link": "{}".format(self.link),
                    "image": "{}".format(self.image),
                    "social media URL": "{}".format(self.socialmediaurls),
                    "profile level": "{}".format(self.profilelevel),
                    "ntee": "{}".format(self.nteecodes),
                    "areas served": "{}".format(self.areasserved),
                    "outbreak_ids": "{}".format(self.outbreaks)
                }

        class Outbreaks(db.Model):
            __table__ = db.Model.metadata.tables["outbreaks"]
            def serialize(self):
                return {
                    "outbreak_id": "{}".format(self.id),
                    "disease": "{}".format(self.disease),
                    "level": "{}".format(self.level),
                    "country": "{}".format(self.location),
                    "description": "{}".format(self.description),
                    "link": "{}".format(self.link),
                    "date": "{}".format(self.date)
                }

        class Diseases(db.Model):
            __table__ = db.Model.metadata.tables["diseases"]
            def serialize(self):
                return {
                    "id": "{}".format(self.id),
                    "name": "{}".format(self.name),
                    "alternate names": "{}".format(self.altnames),
                    "transmission": "{}".format(self.transmission)
                }

        
        self.countries = Countries
        self.organizations = Organizations
        self.diseases = Diseases
        self.outbreaks = Outbreaks


    def get_data(self, route, vals):

        if route == "countries":
            q = self.countries.query

            # countries filtering
            if vals["code"]:
                q = q.filter(self.countries.code == vals["code"])
            if vals["name"]:
                q = q.filter(self.countries.name == vals["name"])
            if vals["region"]:
                q = q.filter(self.countries.region == vals["region"])
            if vals["subregion"]:
                q = q.filter(self.countries.subregion == vals["subregion"])
            if vals["population_min"]:
                q = q.filter(self.countries.population >= int(vals["population_min"]))
            if vals["population_max"]:
                q = q.filter(self.countries.population <= int(vals["population_max"]))
            if vals["mortality_min"]:
                q = q.filter(self.countries.mortality >= int(vals["mortality_min"]))
            if vals["mortality_max"]:
                q = q.filter(self.countries.mortality <= int(vals["mortality_max"]))
            if vals["life_expectancy_min"]:
                q = q.filter(self.countries.lifeexpectancy >= int(vals["life_expectancy_min"]))
            if vals["life_expectancy_max"]:
                q = q.filter(self.countries.lifeexpectancy <= int(vals["life_expectancy_max"]))

            if vals["search"]:
                columns = [self.countries.name,
                            self.countries.region,
                            self.countries.subregion,
                            self.countries.population,
                            self.countries.lifeexpectancy,
                            self.countries.mortality]
                terms = vals["search"].split()
                queries = (q.filter(func.lower(cast(column, sqlalchemy.String)).like('%' + term.lower() + '%')) for column in columns for term in terms)
                q = q.filter(False).union(*queries)

            if vals["sort"]:
                asc = (vals["sort"].split("sc-")[0] == "a")
                sort_val = vals["sort"].split("sc-")[1].lower()
                if asc:
                    if sort_val == "code":
                        q = q.order_by(self.countries.code)
                    elif sort_val == "name":
                        q = q.order_by(self.countries.name)
                    elif sort_val == "region":
                        q = q.order_by(self.countries.region)
                    elif sort_val == "subregion":
                        q = q.order_by(self.countries.subregion)
                    elif sort_val == "population":
                        q = q.order_by(self.countries.population)
                    elif sort_val == "area":
                        q = q.order_by(self.countries.area)
                    elif sort_val == "lifeexpectancy":
                        q = q.order_by(self.countries.lifeexpectancy)
                    elif sort_val == "mortality":
                        q = q.order_by(self.countries.mortality)
                else:
                    if sort_val == "code":
                        q = q.order_by(self.countries.code.desc())
                    elif sort_val == "name":
                        q = q.order_by(self.countries.name.desc())
                    elif sort_val == "region":
                        q = q.order_by(self.countries.region.desc())
                    elif sort_val == "subregion":
                        q = q.order_by(self.countries.subregion.desc())
                    elif sort_val == "population":
                        q = q.order_by(self.countries.population.desc())
                    elif sort_val == "area":
                        q = q.order_by(self.countries.area.desc())
                    elif sort_val == "lifeexpectancy":
                        q = q.order_by(self.countries.lifeexpectancy.desc())
                    elif sort_val == "mortality":
                        q = q.order_by(self.countries.mortality.desc())

        elif route == "outbreaks":
            q = self.db.session.query(self.outbreaks, self.diseases).outerjoin(self.diseases)

            # outbreaks filtering
            if vals["outbreak_id"]:
                q = q.filter(self.outbreaks.id == vals["outbreak_id"])
            if vals["disease_name"]:
                q = q.filter(self.diseases.name == vals["disease_name"])
            if vals["country"]:
                q = q.filter(self.outbreaks.location == vals["country"])
            if vals["start_date_min"]:
                d = vals["start_date_min"]
                p = [int(s) for s in (d[6:10], d[0:2], d[3:5])]
                s = date(*p)
                q = q.filter(self.outbreaks.date >= s)
            if vals["start_date_max"]:
                d = vals["start_date_max"]
                p = [int(s) for s in (d[6:10], d[0:2], d[3:5])]
                s = date(*p)
                q = q.filter(self.outbreaks.date <= s)

            if vals["search"]:
                columns = [self.outbreaks.disease,
                            self.outbreaks.location,
                            self.outbreaks.description,
                            self.outbreaks.date,
                            # self.diseases.id,
                            # self.diseases.name,
                            # self.diseases.altnames,
                            # self.diseases.transmission,
                            ]
                terms = vals["search"].split()
                queries = (q.filter(func.lower(cast(column, sqlalchemy.String)).like('%' + term.lower() + '%')) for column in columns for term in terms)
                q = q.filter(False).union(*queries)

            if vals["sort"]:
                asc = (vals["sort"].split("sc-")[0] == "a")
                sort_val = vals["sort"].split("sc-")[1].lower()
                if asc:
                    if sort_val == "disease":
                        q = q.order_by(self.outbreaks.disease)
                    elif sort_val == "location":
                        q = q.order_by(self.outbreaks.location)
                else:
                    if sort_val == "disease":
                        q = q.order_by(self.outbreaks.disease.desc())
                    elif sort_val == "location":
                        q = q.order_by(self.outbreaks.location.desc())

        elif route == "organizations":
            q = self.organizations.query

            # organizations filtering
            if vals["org_id"]:
                q = q.filter(self.organizations.id == vals["org_id"])
            if vals["outbreak_id"]:
                v = int(vals["outbreak_id"])
                q = q.filter(self.organizations.outbreaks.any(v))
            if vals["area"]:
                q = q.filter(self.organizations.areasserved.any(vals["area"]))
            if vals["profile_level"]:
                q = q.filter(self.organizations.profilelevel == vals["profile_level"])

            if vals["search"]:
                columns = [self.organizations.name,
                            self.organizations.mission,
                            self.organizations.link,
                            self.organizations.profilelevel,
                            ]
                terms = vals["search"].split()
                queries = (q.filter(func.lower(cast(column, sqlalchemy.String)).like('%' + term.lower() + '%')) for column in columns for term in terms)
                # I'm having problems doing search on the list fields
                # areas_string = ', '.join(map(str, self.organizations.areasserved))
                # areas_queries = (q.filter(func.lower(areas_string).like('%' + term.lower() + '%')) for term in terms)
                # ntee_string = ', '.join(map(str, self.organizations.nteecodes))
                # ntee_queries = (q.filter(func.lower(ntee_string).like('%' + term.lower() + '%')) for term in terms)
                # q = q.filter(False).union(*areas_queries, *ntee_queries, *queries).distinct(self.organizations.id)
                q = q.filter(False).union(*queries)

            if vals["sort"]:
                asc = (vals["sort"].split("sc-")[0] == "a")
                sort_val = vals["sort"].split("sc-")[1].lower()
                if asc:
                    if sort_val == "name":
                        q = q.order_by(self.organizations.name)
                    elif sort_val == "level" or sort_val == "profilelevel":
                        q = q.order_by(self.organizations.profilelevel)
                else:
                    if sort_val == "name":
                        q = q.order_by(self.organizations.name.desc())
                    elif sort_val == "level" or sort_val == "profilelevel":
                        q = q.order_by(self.organizations.profilelevel.desc())

        page, per_page = 1, 100
        if vals["page"] and vals["per_page"]:
            page, per_page = int(vals["page"]), int(vals["per_page"])

        p = q.paginate(page, per_page, error_out=False)
        results = None
        if route == "outbreaks":
            results = [{**u.serialize(), **v.serialize()} for u,v in p.items]
        else:
            results = [v.serialize() for v in p.items]
        #TODO: use jsonify
        return json.dumps(results)
