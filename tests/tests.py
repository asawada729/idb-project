# Atsuki: 2 tests
# David: 0 tests
# Kevin: 0 tests
# Pranay: 2 tests
# Vishal: 9 tests

from unittest import main, TestCase
import requests

class UnitTests(TestCase):
    
    def test_pagination(self):
        URL_countries = "http://epidemicrelief-api.appspot.com/countries"
        URL_outbreaks = "http://epidemicrelief-api.appspot.com/outbreaks"
        URL_organizations = "http://epidemicrelief-api.appspot.com/organizations"
        
        PARAMS = {"page": 1, "per_page": 1}
        
        result_countries = requests.get(url=URL_countries, params=PARAMS).json()
        result_outbreaks = requests.get(url=URL_outbreaks, params=PARAMS).json()
        result_organizations = requests.get(url=URL_organizations, params=PARAMS).json()
        
        assert len(result_countries) == PARAMS["per_page"]
        assert len(result_outbreaks) == PARAMS["per_page"]
        assert len(result_organizations) == PARAMS["per_page"]

    def test_query_by_params1(self):
        URL_countries = "http://epidemicrelief-api.appspot.com/countries"
        URL_outbreaks = "http://epidemicrelief-api.appspot.com/outbreaks"
        URL_organizations = "http://epidemicrelief-api.appspot.com/organizations"

        PARAMS_countries = {"code": 'AFG'}
        PARAMS_outbreak = {"outbreak_id": 1}
        PARAMS_organizations = {"org_id": 1}

        result_countries = requests.get(url=URL_countries, params=PARAMS_countries).json()
        result_outbreaks = requests.get(url=URL_outbreaks, params=PARAMS_outbreak).json()
        result_organizations = requests.get(url=URL_organizations, params=PARAMS_organizations).json()

        assert len(result_countries) == 1
        assert len(result_outbreaks) == 1
        assert len(result_organizations) == 0

        assert result_countries[0]["code"] == "AFG"
    
    def test_query_pagination2(self):
        URL_countries = "http://epidemicrelief-api.appspot.com/countries"
        PARAMS = {"page": 1, "per_page": 10}
        
        result = requests.get(url=URL_countries, params=PARAMS).json()
        result2 = requests.get(url=URL_countries, params=PARAMS).json()
        assert(result == result2)
    
    def test_query_pagination3(self):
        URL_countries = "http://epidemicrelief-api.appspot.com/countries"
        PARAMS = {"page": 1, "per_page": 10}
        PARAMS2 = {"page": 2, "per_page": 10}
        result = requests.get(url=URL_countries, params=PARAMS).json()
        result2 = requests.get(url=URL_countries, params=PARAMS2).json()
        assert(result != result2)
        assert(len(result) == len(result2))

    def test_sort1(self):
        URL_countries = "http://epidemicrelief-api.appspot.com/countries"
        PARAMS = {"sort": "asc-population"}
        result = requests.get(url=URL_countries, params=PARAMS).json()
        assert result[0]["code"] == "ATG"
        assert result[1]["code"] == "SYC"

    def test_sort2(self):
        URL_countries = "http://epidemicrelief-api.appspot.com/countries"
        PARAMS = {"sort": "asc-name"}
        result = requests.get(url=URL_countries, params=PARAMS).json()
        assert result[5]["code"] == "ARG"

    def test_sort3(self):
        URL_orgs = "http://epidemicrelief-api.appspot.com/organizations"
        PARAMS = {"sort": "asc-name"}
        result = requests.get(url=URL_orgs, params=PARAMS).json()
        assert result[0]["name"] == "American National Red Cross"
        assert result[1]["name"] == "Curamericas"

    def test_sort4(self):
        URL_outbreaks = "http://epidemicrelief-api.appspot.com/outbreaks"
        PARAMS = {"sort": "asc-disease"}
        result = requests.get(url=URL_outbreaks, params=PARAMS).json()
        assert result[0]["outbreak_id"] == "11"
        assert result[2]["disease"] == "Measles"

    def test_search1(self):
        URL_outbreaks = "http://epidemicrelief-api.appspot.com/outbreaks"
        PARAMS = {"search": "measles romania"}
        result = requests.get(url=URL_outbreaks, params=PARAMS).json()
        assert result[0]["outbreak_id"] == "6"
    def test_search2(self):
        URL_orgs = "http://epidemicrelief-api.appspot.com/organizations"
        PARAMS = {"search": "gold silver"}
        result = requests.get(url=URL_orgs, params = PARAMS).json()
        assert result[0]["name"] ==  "SUDAN RELIEF FUND INC"

    def test_filter1(self):
        URL_countries = "http://epidemicrelief-api.appspot.com/countries"
        PARAMS = {"name": "Argentina"}
        result = requests.get(url=URL_countries, params=PARAMS).json()
        assert(len(result) == 1)
        assert result[0]["code"] == "ARG"

    def test_filter2(self):
        URL_orgs = "http://epidemicrelief-api.appspot.com/organizations"
        PARAMS = {"profile_level": "Gold"}
        result = requests.get(url=URL_orgs, params=PARAMS).json()
        assert(len(result)==3)
        assert result[0]["name"] == "PATH"

    def test_filter3(self):
        URL_outbreaks = "http://epidemicrelief-api.appspot.com/outbreaks"
        PARAMS = {"disease_name": "Cholera"}
        result = requests.get(url=URL_outbreaks, params=PARAMS).json()
        assert(len(result) == 1)
        assert result[0]["outbreak_id"] == "11"

if __name__ == "__main__":
    main()
