from main import db

#ignore this file for now

class Countries(db.Model):
    __table__ = db.Model.metadata.tables["countries"]

    def serialize(self):
        return {
            "code": "{}".format((self.code).encode("utf-8")),
            "name": "{}".format((self.name).encode("utf-8")),
            "capital": "{}".format((self.capital).encode("utf-8")),
            "region": "{}".format((self.region).encode("utf-8")),
            "subregion": "{}".format((self.subregion).encode("utf-8")),
            "population": "{}".format((self.population).encode("utf-8")),
            "area": "{}".format((self.area).encode("utf-8")),
            "flag": "{}".format((self.flag).encode("utf-8")),
            "life expectancy": "{}".format((self.life_expectancy).encode("utf-8")),
            "mortality": "{}".format((self.mortality).encode("utf-8"))
        }

class Outbreaks(db.Model):
    __table__ = db.Model.metadata.tables["outbreaks"]

class Diseases(db.Model):
    __table__ = db.Model.metadata.tables["diseases"]

class Organizations(db.Model):
    __table__ = db.Model.metadata.tables["organizations"]
