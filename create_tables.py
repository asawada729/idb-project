from main import db

if __name__=="__main__":
    #manually run this file before running main.py if database declared in main.py does not exist
    #this file can be run from any machine to initialize a table
    db.create_all()
    print("database created!")
