import logging
import os
import socket
import json

from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
#from models import Countries, Outbreaks, Diseases, Organizations
from flask_cors import CORS
import sqlalchemy

app=Flask(__name__)
CORS(app)

from sql_queries import Sql

sql = Sql(app)

@app.route("/")
def get_root():
    return "Welcome to the Epidemic Relief API. Consult \
    https://documenter.getpostman.com/view/5487483/RWgm41ZV to find out how \
    to structure your API calls"

@app.route("/countries")
def get_countries():
    query_params = {p: request.args.get(p) for p in ("code", "page", "per_page", "sort", "search", "name", "region", "subregion", "population_min", "population_max", "mortality_min", "mortality_max", "life_expectancy_min", "life_expectancy_max")}
    return sql.get_data("countries", query_params)

@app.route("/organizations")
def get_organizations():
    query_params = {p: request.args.get(p) for p in ("org_id", "page", "per_page", "sort", "search", "outbreak_id", "area", "profile_level")}
    return sql.get_data("organizations", query_params)

@app.route("/outbreaks")
def get_outbreaks():
    query_params = {p: request.args.get(p) for p in ("outbreak_id", "page", "per_page", "sort", "search", "disease_name", "country", "start_date_min", "start_date_max")}
    return sql.get_data("outbreaks", query_params)

@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500

if __name__=="__main__":
    #this is for running on local machine
    app.run(host="127.0.0.1", port=8080, debug=True)
